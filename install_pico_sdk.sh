# install all the dependencies
#sudo pacman -Sy minicom openocd cmake arm-none-eabi-gcc arm-none-eabi-newlib gcc
#
## clone all the base repos
#git clone https://github.com/raspberrypi/pico-sdk.git
#git clone https://github.com/raspberrypi/pico-examples.git
#git clone https://github.com/raspberrypi/pico-extras.git
#git clone https://github.com/raspberrypi/pico-playground.git
#
## Update these paths to match where you've checked out the repos
#echo 'export PICO_SDK_PATH=${HOME}/pico-sdk' >> ~/.zshrc
#echo 'export PICO_EXTRAS_PATH=${HOME}/pico-extras' >> ~/.zshrc
#
## read in the env variables we just set
#source ~/.zshrc 
#
# pull in the git submodule dependencies in the pico-sdk and pico-extras projects
cd pico-sdk
git submodule update --init
cd ../pico-extras
git submodule update --init
cd ..

# Optional, haven't tried these yet
# git clone https://github.com/raspberrypi/picoprobe.git
# git clone https://github.com/raspberrypi/picotool.git


## example for how to build from command prompt:
#cd pico_examples
#mkdir build
#cd build
#cmake ../ -DCMAKE_BUILD_TYPE=Debug
#
#cd blink
#make -j6 # you can update this 4 to be how many CPU cores you want to build on
