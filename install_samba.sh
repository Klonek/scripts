#!/bin/bash

# Check if running as root
if [ "$EUID" -ne 0 ]; then 
    echo "Please run as root or with sudo"
    exit 1
fi

# Get local IP address
LOCAL_IP=$(ip -4 addr show | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep -v '127.0.0.1' | head -n 1)
NETWORK_SUBNET=$(echo $LOCAL_IP | cut -d. -f1-3).0/24

echo "Using IP address: $LOCAL_IP"
echo "Network subnet: $NETWORK_SUBNET"

# Install Samba
echo "Installing Samba..."
pacman -Syu samba

# Enable and start Samba
echo "Enabling and starting Samba service..."
systemctl enable --now smb

# Configure Samba
echo "Configuring Samba..."
cat > /etc/samba/smb.conf << EOL
[global]
workgroup = WORKGROUP
server string = Samba Server
security = user
printing = CUPS
printcap name = CUPS

[printers]
comment = All Printers
browseable = yes
path = /var/spool/samba
printable = yes
guest ok = yes
read only = yes
create mask = 0700
EOL

# Configure CUPS
echo "Configuring CUPS..."
sed -i "s|Listen localhost:631|Listen $LOCAL_IP:631|g" /etc/cups/cupsd.conf

# Add browsing configuration
sed -i '/# Show shared printers on the local network./a Browsing Yes' /etc/cups/cupsd.conf

# Configure CUPS access
cat > /etc/cups/cupsd.conf.new << EOL
# CUPS configuration file
ServerName $LOCAL_IP
Listen $LOCAL_IP:631
Browsing Yes
BrowseLocalProtocols dnssd

# Default authentication type
DefaultAuthType Basic
WebInterface Yes

<Location />
  Order allow,deny
  Allow localhost
  Allow from $NETWORK_SUBNET
</Location>

<Location /admin>
  Order allow,deny
  Allow localhost
  Allow from $NETWORK_SUBNET
</Location>

<Location /admin/conf>
  AuthType Default
  Require valid-user
  Order allow,deny
  Allow localhost
  Allow from $NETWORK_SUBNET
</Location>
EOL

mv /etc/cups/cupsd.conf.new /etc/cups/cupsd.conf

# Restart services
echo "Restarting services..."
systemctl restart smb
systemctl restart cups

echo "Printer sharing setup completed!"
echo "Your printer should now be available on the network at $LOCAL_IP"
echo "Note: You may need to install printer drivers on client machines"
echo "To check CUPS status, visit: http://$LOCAL_IP:631"
