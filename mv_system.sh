#!/bin/bash
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "FAIL: \"$last_command\" command fail with exit code $?."' EXIT

from=$1
to=$2
echo 'Moving partition from $from to $to...'


