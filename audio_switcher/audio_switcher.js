//#!/usr/bin/env zx
import { $, log, sleep, which } from "zx";
import http from "http";
import inquirer from "inquirer";
import tk from "terminal-kit";
const { terminal } = tk;

const soundCardsList = async () =>
  await $`pactl list cards short`
    .then((output) =>
      output.stdout
        .split("\n")
        .filter((line) => line.trim() !== "")
        .map((line) => ({ id: line.split("\t").shift() })),
    )
    .catch(() => 0);
const soundCardDetail = async (cardId) =>
  await $`pactl list cards | grep -A 100 "Card #${cardId}"`
    .then((output) =>
      output.stdout.split("\n").filter((line) => line.trim() !== ""),
    )
    .then((output) => output.splice(1, output.length))
    .then((output) =>
      output.splice(
        0,
        output.findLastIndex((item) => !item.startsWith("\t")) !== -1
          ? output.findLastIndex((item) => !item.startsWith("\t"))
          : output.length,
      ),
    )
    .then((output) => ({
      id: cardId,
      description: output
        .find((line) => line.includes("device.description"))
        .split("=")[1]
        .replaceAll('"', "")
        .trim(),
    }))

    .catch(() => 0);
const sounCardOutputs = async (cardId) =>
  await $`pactl list cards | grep -A 100 "Card #${cardId}" | grep "Profiles:" -A 20 | sed 's/ ([^)]*)$//' | grep -E "^\s+output|^\s+in
put|^\s+pro-audio"`
    .then((output) =>
      output.stdout.split("\n").filter((line) => line.trim() !== ""),
    )
    .then((output) =>
      output
        .splice(
          0,
          output.findLastIndex((item) => !item.startsWith("\t\t")) !== -1
            ? output.findLastIndex((item) => !item.startsWith("\t\t"))
            : output.length,
        )
        .map((item) => item.slice(2, item.length))
        .map((item) => ({
          output: item.slice(0, item.indexOf(" ") - 1),
          name: item.slice(item.indexOf(" "), item.length),
        })),
    )
    .catch(() => 0);
const setProfile = async (cardId, profile) =>
  await $`pactl set-card-profile ${cardId} ${profile}`
    .then((output) => output.stdout)
    .catch(() => 0);
const displayOutputs = (cards) => {
  cards.map((card) => {
    console.log(`${card.id} ${card.description}`);
    card.outputs.map((output) => { });
  });
};
let cards = await Promise.all(
  (await soundCardsList()).map(
    async (item) =>
      await {
        ...item,
        ...(await soundCardDetail(item.id)),
        outputs: await sounCardOutputs(item.id),
      },
  ),
);
const promptUser = async (cards) => {
  console.clear();
  let backToCardSelection = true;

  while (backToCardSelection) {
    // Etap 1: Wybór karty
    const cardChoices = cards.map((card) => card.description);
    terminal.grabInput({ mouse: "button" });

    terminal.singleColumnMenu(
      cardChoices,
      { exitOnUnexpectedKey: true },
      (error, response) => {
        const selectedCard = cards[response.selectedIndex];
        console.clear();
        let backToOutputSelection = true;

        const outputChoices = selectedCard.outputs
          .map((output) => output.name)
          .concat(["Wróć do wyboru karty"]);

        terminal.singleColumnMenu(
          outputChoices,
          { exitOnUnexpectedKey: true },
          async (error, response) => {
            if (response.selectedIndex === selectedCard.outputs.length) {
              // Użytkownik wybrał "Wróć do wyboru karty"
              backToOutputSelection = false;
              terminal.clear();
              promptUser();
            } else {
              const selectedOutput =
                selectedCard.outputs[response.selectedIndex];
              if (selectedOutput) {
                const setprof = await setProfile(
                  selectedCard.id,
                  selectedOutput.output,
                );
                process.exit();
              }
            }
          },
        );
      },
    );

    backToCardSelection = false;
  }
};

terminal.on("key", (key) => {
  if (key === "CTRL_C") {
    terminal.grabInput(false);
    setTimeout(() => {
      process.exit();
    }, 100);
  }
});

promptUser(cards);
