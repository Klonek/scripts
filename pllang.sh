#!/bin/zsh

# Aktualizacja systemu
sudo pacman -Syu

# Instalacja pakietów językowych
sudo pacman -S man-pages-pl glibc

# Edycja pliku locale.gen
sudo sed -i 's/#pl_PL.UTF-8 UTF-8/pl_PL.UTF-8 UTF-8/' /etc/locale.gen
sudo sed -i 's/#pl_PL ISO-8859-2/pl_PL ISO-8859-2/' /etc/locale.gen

# Generowanie lokalizacji
sudo locale-gen

# Ustawienie domyślnej lokalizacji
echo 'LANG=pl_PL.UTF-8' | sudo tee /etc/locale.conf

# Ustawienie lokalizacji dla sesji użytkownika
echo 'export LANG=pl_PL.UTF-8' >> ~/.zshrc

# Czyszczenie pliku vconsole.conf
echo '' | sudo tee /etc/vconsole.conf

# Konfiguracja klawiatury dla terminala (TTY)
echo 'KEYMAP=pl' | sudo tee -a /etc/vconsole.conf
echo 'FONT=Lat2-Terminus16.psfu.gz' | sudo tee -a /etc/vconsole.conf
echo 'FONT_MAP=8859-2' | sudo tee -a /etc/vconsole.conf

# Konfiguracja i3
mkdir -p ~/.config/i3/config.d
cat <<EOL > ~/.config/i3/config.d/language.conf
set \$LANG pl_PL.UTF-8
exec_always --no-startup-id setxkbmap -layout pl
EOL

# Restart i3
i3-msg restart
