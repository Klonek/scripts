#!/bin/bash
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "FAIL: \"$last_command\" command fail with exit code $?."' EXIT

echo 'Updating official packages'
sudo pacman -Syu --noconfirm

echo 'updating AUR packages'
yay -Syu --noconfirm --sudoloop

##TODO: Updating docker 
echo 'System fully updated. You should reboot system'

