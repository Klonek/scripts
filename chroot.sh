#!/bin/zsh

# Sprawdzenie, czy argument został podany
if [ -z "$1" ]; then
  echo "Użycie: $0 /dev/dysk"
  exit 1
fi

DISK=$1

# Montowanie systemu plików
mount $DISK /mnt
if [ $? -ne 0 ]; then
  echo "Nie udało się zamontować $DISK"
  exit 1
fi

mount /dev/sda1 /mnt/boot/efi
if [ $? -ne 0 ]; then
  echo "Nie udało się zamontować /dev/sda1 na /mnt/boot/efi"
  exit 1
fi

mount --bind /proc /mnt/proc
if [ $? -ne 0 ]; then
  echo "Nie udało się zamontować /proc"
  exit 1
fi

mount --bind /dev /mnt/dev
if [ $? -ne 0 ]; then
  echo "Nie udało się zamontować /dev"
  exit 1
fi

mount --bind /sys /mnt/sys
if [ $? -ne 0 ]; then
  echo "Nie udało się zamontować /sys"
  exit 1
fi

# Kopiowanie pliku resolv.conf
cp /etc/resolv.conf /mnt/etc/
if [ $? -ne 0 ]; then
  echo "Nie udało się skopiować /etc/resolv.conf"
  exit 1
fi

# Wejście do chroot
chroot /mnt
if [ $? -ne 0 ]; then
  echo "Nie udało się wejść do chroot"
  exit 1
fi
