#!/bin/bash

#INSTALING FONTS
echo 'installing required fonts'
git clone --depth=1 https://github.com/ryanoasis/nerd-fonts.git ~/fonts
cd ~/fonts
./install.sh
cd ~
rm -Rf fonts

#Instaling powerlevel10k zsh theme
echo 'installing powerlevel10k'
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k

#setting Up
echo 'setting up powelevel10k'
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
source ~/.zshrc
