#!/bin/bash
authorized_keys_file="/home/ansible/.ssh/authorized_keys"
ssh_key="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHTB9LeppwNKcfiDJQ/SB7Uo2raIXW/y0M2nOBt49c5g ansible@futro"
sudoers_file="/etc/sudoers"
premissions="Defaults:ansible !authenticate"

if [ "$EUID" -ne 0 ]; then
    echo "Skrypt musi być uruchamiany z uprawnieniami roota (sudo)"
    exit 1
fi

#Dodawanie uprawnień sudo
sudo useradd -m -G wheel -s /bin/zsh ansible
echo "$premissions" >> "$sudoers_file"
if [ $? -eq 0 ]; then
    echo "Uprawnienia dodane do pliku sudoers."
else
    echo "Wystąpił błąd podczas dodawania uprawnień."
fi
sudo mkdir /home/ansible/.ssh
# Adding ssh key
echo "$ssh_key" >> "$authorized_keys_file"
if [ $? -eq 0 ]; then
    echo "Klucz SSH został dodany do pliku authorized_keys."
else
    echo "Wystąpił błąd podczas dodawania klucza."
fi
